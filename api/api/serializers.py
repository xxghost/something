#! /usr/bin/env python
# coding=utf-8
# author: Rand01ph

from django.contrib.auth.models import User, Group
from rest_framework import serializers
from api.models import Personal, Invite, Rating


class PersonInfoCreateSerializer(serializers.Serializer):
    name = serializers.CharField()
    address = serializers.CharField()
    phone = serializers.CharField()
    sex = serializers.CharField()
    skills = serializers.ListField()
    items = serializers.ListField()

    def create(self, validated_data):
        return Personal.objects.create(**validated_data)


class PersonInfoDetailSerializer(serializers.Serializer):
    name = serializers.CharField()
    address = serializers.CharField()
    phone = serializers.CharField()
    sex = serializers.CharField()
    skills = serializers.ListField()
    items = serializers.ListField()

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.address = validated_data.get('address', instance.address)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.sex = validated_data.get('sex', instance.sex)
        instance.skills = validated_data.get('skills', instance.skills)
        instance.items = validated_data.get('items', instance.items)
        return instance


class InviteCreateSerializer(serializers.Serializer):
    inviter = serializers.IntegerField()
    invitee_something = serializers.CharField()

    def create(self, validated_data):
        return Invite.objects.create(**validated_data)


class InvitationInfoSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    inviter = serializers.IntegerField()
    invitee = serializers.IntegerField(required=False)
    inviter_something = serializers.CharField(required=False)
    invitee_something = serializers.CharField()
    #1 do nothing 2 accept 3 not accept
    invitation_status = serializers.IntegerField()

    def update(self, instance, validated_data):
        instance.inviter_something = validated_data.get('inviter_something', instance.inviter_something)
        instance.invitation_status = validated_data.get('invitation_status', instance.invitation_status)
        return instance


# class InvitationAcceptSerializer(serializers.Serializer):
#     inviter = serializers.IntegerField()
#     invitee = serializers.IntegerField(required=False)
#     inviter_something = serializers.CharField(required=False)
#     invitee_something = serializers.CharField()

#     def create(self, validated_data):
#         return Invite.objects.create(**validated_data)

#     def update(self, instance, validated_data):
#         instance.inviter = validated_data.get('inviter', instance.inviter)
#         instance.invitee = validated_data.get('invitee', instance.invitee)
#         instance.inviter_something = validated_data.get('inviter_something', instance.inviter_something)
#         instance.invitee_something = validated_data.get('invitee_something', instance.invitee_something)
#         return instance


class RatingCreateSerializer(serializers.Serializer):
    score_person = serializers.IntegerField()
    judged_person = serializers.IntegerField()
    score = serializers.IntegerField()
