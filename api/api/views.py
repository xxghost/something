#! /usr/bin/env python
# coding=utf-8
# author: Rand01ph

from django.db.models import Q
from rest_framework import mixins, generics
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from api.models import Personal, Invite, Rating
from api.serializers import PersonInfoCreateSerializer, PersonInfoDetailSerializer, InviteCreateSerializer, InvitationInfoSerializer

class PersonalInfoCreateAPIView(mixins.CreateModelMixin,
                                mixins.ListModelMixin,
                                generics.GenericAPIView):

    queryset = Personal.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return PersonInfoCreateSerializer
        elif self.request.method == 'GET':
            return PersonInfoCreateSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(score=0)


class PersonalInfoDetailAPIView(mixins.RetrieveModelMixin,
                                generics.GenericAPIView):

    queryset = Personal.objects.all()
    lookup_url_kwarg = 'info_id'

    def get_serializer_class(self):
        if self.request.method == 'PUT':
            return PersonInfoDetailSerializer
        elif self.request.method == 'GET':
            return PersonInfoDetailSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class InvitationInfoAPIView(mixins.CreateModelMixin,
                            mixins.ListModelMixin,
                            generics.GenericAPIView):

    queryset = Invite.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return InviteCreateSerializer
        elif self.request.method == 'GET':
            return InvitationInfoSerializer

    def get(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        people_num = self.perform_create(serializer)
        return Response("There %s people notify" % people_num)

    def perform_create(self, serializer):
        data = serializer.validated_data
        inviter = data.get('inviter')
        invitee_something = data.get('invitee_something')
        invitee = Personal.objects.filter(Q(skills__contains=invitee_something) | Q(items__contains=invitee_something))
        for i in invitee:
            Invite.objects.create(inviter=inviter, invitee=i.id, invitee_something=invitee_something, invitation_status=1)
        return len(invitee)

class InvitationInfoDetailAPIView(mixins.RetrieveModelMixin,
                                  mixins.UpdateModelMixin,
                                  generics.GenericAPIView):

    queryset = Invite.objects.all()
    lookup_url_kwarg = 'info_id'

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return InvitationInfoSerializer
        elif self.request.method == 'PUT':
            return InvitationInfoSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def put(self, request, *args, **kwargs):
        print request
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
