#! /usr/bin/env python
# coding=utf-8
# author: Rand01ph

from django.db import models
from django_mysql.models import JSONField, Model, ListTextField

# Create your models here.
class Personal(Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=256)
    phone = models.CharField(max_length=11)
    sex = models.CharField(max_length=1)
    skills = ListTextField(base_field=models.CharField(max_length=10), size=100,)
    items = ListTextField(base_field=models.CharField(max_length=10), size=100,)
    score = models.IntegerField()


class Invite(Model):
    # yaoqingren
    inviter = models.IntegerField()
    # bei yao qing
    invitee = models.IntegerField()
    # yaoqingren techang
    inviter_something = models.CharField(max_length=30, null=True, blank=True)
    invitee_something = models.CharField(max_length=30)
    #1 do nothing 2 accept 3 not accept
    invitation_status = models.IntegerField()


class Rating(Model):
    score_person = models.IntegerField()
    judged_person = models.IntegerField()
    score = models.IntegerField()
