from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^personal/info/$', views.PersonalInfoCreateAPIView.as_view(), name='personal-info'),
    url(r'^personal/info/(?P<info_id>[0-9]+)/$', views.PersonalInfoDetailAPIView.as_view(), name='personal-info'),
    # chaxun invite
    url(r'^invitation/info/$', views.InvitationInfoAPIView.as_view(), name='invitation-info'),
    url(r'^invitation/info/(?P<info_id>[0-9]+)/$', views.InvitationInfoDetailAPIView.as_view(), name='invitation-info'),
]
